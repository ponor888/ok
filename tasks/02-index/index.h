#pragma once

#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>


namespace codesearch {

////////////////////////////////////////////////////////////////////////////////

using DocumentId = uint32_t;

////////////////////////////////////////////////////////////////////////////////

struct Document {
    std::string path;
    std::string text;
};

////////////////////////////////////////////////////////////////////////////////

struct IndexedDocument {
    DocumentId id = 0;
    std::string path;
    std::string text;
};

////////////////////////////////////////////////////////////////////////////////

struct IndexOptions {
    size_t arity = 3;
};

////////////////////////////////////////////////////////////////////////////////

class Index {
    friend class IndexBuilder;

public:
    Index(IndexOptions opts) {
        // Your code goes here
    }

public:
    int Arity() const {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

    const std::vector<DocumentId>* LookupNgram(const std::string& ngram) const {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

    DocumentId DocumentCount() const {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

    const IndexedDocument& GetDocument(DocumentId id) const {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

private:
    // Your code goes here
};

////////////////////////////////////////////////////////////////////////////////

class IndexBuilder {
public:
    IndexBuilder(IndexOptions opts) {
        // Your code goes here
    }

public:
    void AddDocument(Document document) {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

    Index Finish() && {
        // Your code goes here
        throw std::runtime_error{"Not implemented"};
    }

private:
    // Your code goes here
};

////////////////////////////////////////////////////////////////////////////////

} // namespace codesearch
