# Настройка окружения на Windows

Здесь описано, как настроить окружение на Windows с использованием WSL (Windows Subsystem from Linux) и VSC (Visual Studio Code) или Vim.

1. Установите WSL2 [по инструкции](https://docs.microsoft.com/ru-ru/windows/wsl/install-win10#manual-installation-steps). Дистрибутив лучше взять Ubuntu 22.04.
2. Запустите WSL2 и установите в ней необходимые инструменты (компилятор, отладчик, CMake):

```
sudo apt update
sudo apt install -y g++-12 gdb cmake
sudo ln -s /usr/bin/g++-12 /usr/bin/g++
```

После убедитесь, что все установилось как надо: `cmake --version` должен выдавать версию не ниже 3.13, `g++ --version`--- не ниже 10.3.

3. Если вы решили использовать Vim, он уже должен быть доступен. Можете настраивать как обычно.

4. Если вы решили использовать VSC, то вам нужно следовать [этой инструкции](https://code.visualstudio.com/docs/remote/wsl). После установки плагина просто выполните `code .` в директории проекта. В итоге, если вы в VSC откроете консоль (`Terminal->New Terminal`), префикс должен быть похож на `username@device:~/cpp$`.

5. Запустите что-нибудь, чтобы убедиться, что все работает.
