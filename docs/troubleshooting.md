# Распространённые проблемы с окружением

## Не получается сдать задачу

Если у вас завершается с ошибкой скрипт отправки решения `submit.py`, первое, что нужно сделать - это запустить его с опцией `-v` (verbose), которая даст подробный вывод и позволит выяснить источник проблемы: `python3 ../../../submit.py -v`.

## `ssh: connect to host gitlab.cpp-hse.net port 2224: Connection refused`

Скорее всего, вы находитесь в необычной сети, например, в сети ВШЭ, в которой не разрешены соединения к некоторым портам. Попробуйте сменить сеть.

## `git@gitlab.cpp-hse.net: Permission denied (publickey)` при попытке сдать задачу

Это означает, что публичный SSH-ключ либо не прописан на Gitlab, либо прописан с ошибкой. Посмотреть список зарегистрированных SSH ключей можно в настройках на gitlab.cpp-hse.net (`Preferences` -> `SSH Keys` -> `Your SSH keys`). Там же можно добавить новый ключ.

Если вы работаете с репозиторием на виртуальной машине, то генерировать SSH-ключ нужно также на ней.

Если вы уверены, что добавили правильный ключ, но ошибка не уходит, то можно сравнить SHA256 хэши ключа на Gitlab (указан на странице ключа в настройках) с тем, что используется при аутентификации (можно найти в выводе `ssh -v git@gitlab.cpp-hse.net` в строчке вида `Will attempt key...`).

## `fatal: No such remote 'student'` при попытке сдать задачу

Выполните `git remote -v`. Вывод должен выглядеть так:

```
origin  https://gitlab.com/BigRedEye1/search-crashcourse (fetch)
origin  https://gitlab.com/BigRedEye1/search-crashcourse (push)
student ssh://git@gitlab.cpp-hse.net:2224/sirius-2023/sirius-Name-Surname-Login.git (fetch)
student ssh://git@gitlab.cpp-hse.net:2224/sirius-2023/sirius-Name-Surname-Logingit (push)
```

Важно, что у `student` должен быть указан именно SSH URL (начинается с `git@gitlab.cpp-hse.net`). Если у вас что-то не так, то пересоздайте remote:
```
git remote remove student
git remote add student ssh://git@gitlab.cpp-hse.net:2224/sisius-2023/sisius-Name-Surname-Login.git
```
